function getDay(days, today) {
    var thisday = null;
    days.forEach(function(testday) {
        if (testday.date.length === 0 && testday.day.length === 0) thisday = testday;
        else {
            if (testday.day.indexOf(today.getDay()) > -1) thisday = testday;
            if (testday.date.indexOf((today.getMonth() + 1) + "-" + today.getDate() + "-" + today.getFullYear()) >
                -1) thisday = testday;
        }
    });
    return thisday;
}
function getNextAlarm(alarms, time) {
    var nextAlarm = null;
    alarms.forEach(function(testalarm) {
        if (nextAlarm !== null) return;
        var testalarmdate = new Date(time);
        testalarmdate.setHours(testalarm.time[0]);
        testalarmdate.setMinutes(testalarm.time[1]);
        testalarmdate.setSeconds(testalarm.time[2]);
        if (testalarmdate.getTime() > time.getTime()) nextAlarm = testalarm;
    });
    return nextAlarm;
}