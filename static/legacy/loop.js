var loopid = -1;
function startloop(speed, operator) {
    if (loopid != -1) {
        stoploop();
    }
    loopid = setInterval(operator, speed);
}
function stoploop() {
	clearInterval(loopid);
}