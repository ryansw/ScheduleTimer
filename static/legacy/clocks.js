//Begin Clock Manager
var clocks = new Map();
function updateClocks() {
    clocks.forEach(function(time, id) {
        var selector = ".clock#" + id + " ";
        var sec = time.getSeconds(); //Math.floor(time / 1000    % 60);
        var min = time.getMinutes(); //Math.floor(time / 60000   % 60);
        var hrs = time.getHours(); //Math.floor(time / 2400000 % 24);
        $(selector + ".sec").text(sec < 10 ? "0" + sec : sec);
        $(selector + ".min").text(min < 10 ? "0" + min : min);
        if (hrs === 0) {
            if ($(".hide-hrs" + selector + ".hrs-disp").is(":visible")) $(".hide-hrs" + selector +
                ".hrs-disp").hide();
        } else {
            if (!$(".hide-hrs" + selector + ".hrs-disp").is(":visible")) $(".hide-hrs" + selector +
                ".hrs-disp").show();
        }
        $(selector + ".hrs").text(hrs < 10 ? "0" + hrs : hrs);
    });
}
//End Clock Manager
//Begin Colon Flasher
var colonstate = false;
function toggleColons() {
    colonstate = !colonstate;
    $(".clock.flash .col").toggleClass("disabled");
}
//End Colon Flasher