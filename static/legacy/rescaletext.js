var rescale = [];
function rescaletext(item) {
    item = "#" + item;
    var maxwidth = $(item).width();
    var maxheight = $(item).height() - 10;
    var itemtext = item + "text";
    if (!($(item).is(":visible") && $(itemtext).is(":visible"))) return;
    while (maxwidth - $(itemtext).width() > 0.5 && maxheight - $(itemtext).height() > 0.5) {
        $(item).css("font-size", (parseFloat($(item).css("font-size")) + 1) + "px");
    }
    while ($(itemtext).width() - maxwidth > 0.5 || $(itemtext).height() - maxheight > 0.5) {
        $(item).css("font-size", (parseFloat($(item).css("font-size")) - 1) + "px");
    }
}
function rescaletextAll() {
    rescale.forEach(rescaletext);
    if ($(document).width() < 150) alert("The display is not meant to work at this width");
}