var offset = 0; //The Time Offset
//Synchronize global time adjustment variable with server
function adjustTimeOffset() {
	//var time = $.parseJSON($.ajax({async:false,url:"./gettime.php"}).responseText).time;
	//offset = new Date().getTime() - time;
}
//Deliver a Date object of the accurate time
function getCurrentTime() {
    return new Date(new Date().getTime() - offset);
}
//Adjust offset to spoof a past or future date
function offsetSpoofDate(time) { //Can only be called after adjustTimeOffset()
    offset += getCurrentTime().getTime() - time.getTime();
}
function todms() {
    var d = getCurrentTime();
    return d.getMilliseconds() + 1000 * (d.getSeconds() + 60 * (d.getMinutes() + 60 * d.getHours()));
}