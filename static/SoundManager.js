function SoundManager (p_LSItem, p_DefaultPath) {
    this.SoundElement = document.createElement("audio");
    this.p_LSItem = p_LSItem;
    this.SoundElement.oncanplay = function() {
        this.SoundElement.oncanplay = function(){};
//        this.Trigger();
    }.bind(this);
    if(localStorage.getItem(p_LSItem) === null) {
        this.SetNewPath("/static/legacy/timer.wav");        // Temporary port from legacy
    } else {
        this.SoundElement.src = localStorage.getItem(p_LSItem);
    }
    this.onNeedQueue = null;
    this.onReady = null;
    this.onEnd = null;
    this.SoundElement.onpause = function() {
        if(this.onEnd !== null)
            this.onEnd();
    }.bind(this);
    this.SoundElement.preload = true;
    this.SoundElement.load();
    this.Muted = false;
}

SoundManager.prototype.Trigger = function() {
    if(this.Muted) return;
    this.SoundElement.pause();
    this.SoundElement.currentTime = 0;
    this.SoundElement.play();
}

SoundManager.prototype.Queue = function() {
    this.SoundElement.play();
    this.SoundElement.pause();
    this.Initialize();
}

SoundManager.prototype.Initialize = function() {
    this.initilized = false;
    this.SoundElement.onplay = function() {
        this.initilized = true;
        this.SoundElement.pause();
        this.SoundElement.onplay = null;
    }.bind(this);
    this.SoundElement.play();
    setTimeout(function() {
        if(this.initilized) {
        } else {
            if(this.onNeedQueue !== null)
                this.onNeedQueue();
        }
    }.bind(this), 1500);
}

SoundManager.prototype.SetNewPath = function(p_Path) {
    try {
        xmlHttp = new XMLHttpRequest();
        xmlHttp.responseType = "ArrayBuffer";
        var RefBack = this;
        xmlHttp.addEventListener("load", function() {
            var uInt8Array = new Uint8Array(this.response);
            var i = uInt8Array.length;
            var binaryString = new Array(i);
            while (i--)
              binaryString[i] = String.fromCharCode(uInt8Array[i]);
            var data = binaryString.join('');
            var dataOut = "data:" + this.getResponseHeader("content-type") + ";base64," + window.btoa(data);
            RefBack.SoundElement.src = dataOut;
            RefBack.SoundElement.load();
            localStorage.setItem(RefBack.p_LSItem, dataOut);
        });
        xmlHttp.open("GET", p_Path);
        xmlHttp.send();
    } catch(error) {
        console.info("Cannot cache to local storage: IE Response type failure. Fallback to manual sorce selection...");
        this.SoundElement.src = p_Path;
    }
}

SoundManager.prototype.SetFromFile = function(p_file) {
    console.info("Not implemented", p_file);    //Soon... After a settings screen has been implemented
}
