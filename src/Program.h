#ifndef PROGRAM_H
#define PROGRAM_H

#include <string>
#include <map>
#include "HTTPServer/Server.h"
#include "HTTPServer/PageProxy.h"
#include "SystemAdapter.h"

namespace Scheduler {

    class Program : private Scheduler::HTTP::WS::SocketListener {
        public:
            Program(int Port);
            ~Program();
            void RegisterSystem(System* sys);
            void MakeDefaultPath(std::string Path);
            void Run();
            void WSOpen(Scheduler::HTTP::WS::Socket* socket);
        private:
            HTTP::Server m_Server;
            HTTP::Pages::PageProxy* m_Landing;
            std::map<std::string, SystemAdapter*> m_Adapters;

    };

}

#endif // PROGRAM_H
