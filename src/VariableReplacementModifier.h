#ifndef VARIABLEREPLACEMENTMODIFIER_H
#define VARIABLEREPLACEMENTMODIFIER_H

#include <map>
#include "HTTPServer/PageModifier.h"

namespace Scheduler {

    class VariableReplacementModifier : public Scheduler::HTTP::Pages::PageModifier {
        public:
            VariableReplacementModifier(Scheduler::HTTP::Pages::PageResource* res);
            VariableReplacementModifier(Scheduler::HTTP::Pages::PageResource* res, std::map<std::string, std::string> variables);
            ~VariableReplacementModifier();
            std::map<std::string, std::string> m_variables;
        public:
            virtual Scheduler::HTTP::Response ModifyData(Scheduler::HTTP::Request& Req, Scheduler::HTTP::Response Resp);
    };

}

#endif // VARIABLEREPLACEMENTMODIFIER_H
