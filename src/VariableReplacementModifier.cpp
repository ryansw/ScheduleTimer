#include "VariableReplacementModifier.h"

Scheduler::VariableReplacementModifier::VariableReplacementModifier(Scheduler::HTTP::Pages::PageResource* res)
    : Scheduler::HTTP::Pages::PageModifier(res) {
}

Scheduler::VariableReplacementModifier::VariableReplacementModifier(Scheduler::HTTP::Pages::PageResource* res, std::map<std::string, std::string> variables)
    : Scheduler::HTTP::Pages::PageModifier(res) {
        m_variables.insert(variables.begin(), variables.end());
}

Scheduler::VariableReplacementModifier::~VariableReplacementModifier() {
}

Scheduler::HTTP::Response Scheduler::VariableReplacementModifier::ModifyData(Scheduler::HTTP::Request& Req, Scheduler::HTTP::Response Resp) {
    std::string Body = Resp.GetBody();
    for(auto pair : m_variables) {
        std::string key = "$(" + pair.first + ")";
        while(Body.find(key) != std::string::npos)
            Body = Body.replace(Body.find(key), key.length(), pair.second);
    }
    Resp.SetBody(Body);
    return Resp;
}
