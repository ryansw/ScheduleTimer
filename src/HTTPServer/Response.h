#ifndef HTTPRESPONSE_H
#define HTTPRESPONSE_H

#include "../mongoose.h"
#include <string>
#include <map>

namespace Scheduler {

    namespace HTTP {

        class Response {
            public:
                Response(std::string body, int responsecode = 200, std::string mime = "text/html");
                ~Response();
                std::string GetHeaders(bool IncContentLength = true, bool IncMIME = true, bool IncCokies = true);
                std::string GetBody();
                void SetBody(std::string body);
                void SetMime(std::string body);
                void SetResponseCode(int Number);
                void SetCookie(std::string Name, std::string Value);
                void SetHeader(std::string Name, std::string Value);
                std::string GetFullMessage();
                void SendTo(mg_connection* nc);
        private:
                std::string Body;
                int ResponseCode;
                std::string MIME;
                std::map<std::string, std::string> Headers;
                std::map<std::string, std::string> Cookies;
        };
    }

}

#endif // HTTPRESPONSE_H
