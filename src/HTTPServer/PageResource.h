#ifndef PAGERESOURCE_H
#define PAGERESOURCE_H
#include <string>
#include "Request.h"
#include "Response.h"

namespace Scheduler { namespace HTTP { namespace Pages {

    class PageResource {
        public:
            PageResource(std::string Path, bool CatchChild = true);
            virtual ~PageResource();
            virtual Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req) =0;
            virtual bool CheckPath(std::string Path);
        public:
            std::string m_Path;
            bool m_CatchChild;
    };

} } }

#endif // PAGERESOURCE_H
