#include "Response.h"

using namespace Scheduler::HTTP;

Response::Response(std::string body, int responsecode, std::string mime)
    : Body(body), ResponseCode(responsecode),  MIME(mime) {
    Headers["Connection"]="close";
    SetResponseCode(200);
}

Response::~Response() {
}

std::string Response::GetHeaders(bool IncContentLength, bool IncMIME, bool IncCokies) {
    std::string ret;
    if(IncContentLength) ret += "\nContent-Length: " + std::to_string(Body.length());
    if(IncMIME) ret += "\nContent-Type: " + MIME;
    if(IncCokies)
        for(auto C : Cookies)
            ret += "\nSet-Cookie " + C.first + "=" + C.second;
    for(auto H : Headers)
        ret += "\n" + H.first + ": " + H.second;
    ret.erase(0, ret.find_first_of("\n") + 1);
    return ret;
}

std::string Response::GetBody() {
    return Body;
}

void Response::SetCookie(std::string Name, std::string Value) {
    Cookies[Name]=Value;
    if(Value.empty()) Cookies.erase(Cookies.find(Name));
}

void Response::SetHeader(std::string Name, std::string Value) {
    Headers[Name]=Value;
    if(Value.empty()) Headers.erase(Headers.find(Name));
}

void Response::SetBody(std::string body) {
    Body = body;
}

void Response::SetMime(std::string mime) {
    MIME = mime;
}

void Response::SetResponseCode(int Number) {
    ResponseCode = Number;
}

std::string Response::GetFullMessage(){
/*    std::string ret;
    ret += "HTTP/1.1 " + std::to_string(ResponseCode) + " " + ResponseMessage;
    ret += GetHeaders();
    ret += "\n";
    ret += GetBody();
    return ret;*/
    return "HTTP/1.0 501 Not Implemented\nContent-Type: text/plain\n\nThe requested function on the server is not yet implemented.";
}


void Response::SendTo(mg_connection* nc)
{
    mg_send_head(nc, ResponseCode, Body.length(), GetHeaders(false).c_str());
    mg_send(nc, Body.c_str(), Body.length());
}
