#ifndef STATICFOLDERPAGERESOURCEMANAGER_H
#define STATICFOLDERPAGERESOURCEMANAGER_H

#include "PageResource.h" // Base class: Scheduler::HTTP::Pages::PageResource
#include <map>

namespace Scheduler { namespace HTTP { namespace Pages {

    class StaticFolderPageResourceManager : public Scheduler::HTTP::Pages::PageResource {
        public:
            StaticFolderPageResourceManager(std::string Path, std::string Folder);
            ~StaticFolderPageResourceManager();
            virtual Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
        private:
            std::string m_Folder;
            std::map<std::string, std::string> m_Mimes;
            Scheduler::HTTP::Pages::PageResource * m_NotFound;
            Scheduler::HTTP::Pages::PageResource * m_ReadError;
    };

} } }

#endif // STATICFOLDERPAGERESOURCEMANAGER_H
