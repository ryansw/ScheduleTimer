#include "PageResource.h"

using namespace Scheduler::HTTP::Pages;

PageResource::PageResource(std::string Path, bool CatchChild)
    : m_Path(Path), m_CatchChild(CatchChild) {
        
}

PageResource::~PageResource() {
}

bool PageResource::CheckPath(std::string Path) {
    if(m_Path == Path) return true;
    if(m_CatchChild) {
//        if(Path[0] == "/") Path.erase(0, 1);
        return Path.substr(0, Path.find_first_of("/")) == m_Path;
    }
    return false;
}
