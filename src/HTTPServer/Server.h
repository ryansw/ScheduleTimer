#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <string>
#include <map>
#include <vector>
#include "PageResourceManager.h" // Base class: Scheduler::PageResourceManager
#include "Socket.h"
#include "SocketListener.h"
#include "../mongoose.h"

namespace Scheduler {
    
    namespace HTTP {
        
        class Server : public Scheduler::HTTP::Pages::PageResourceManager {
            public:
                Server(int Port);
                ~Server();
                void Poll(int time = 1000);
                void PollUntil(std::string path, int time = 1000);
                void RegisterSocketListener(WS::SocketListener* Listener);
                void UnregisterSocketListener(WS::SocketListener* Listener);
            private:
                mg_mgr Manager;
                mg_connection* ServerConn;
                static void ev_handler(mg_connection* nc, int ev, void* ev_data);
                std::map<mg_connection*, WS::Socket*> m_Sockets;
                std::vector<WS::SocketListener*> m_Listeners;
        };
    
    }

}

#endif // HTTPSERVER_H
