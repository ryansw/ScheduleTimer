#ifndef PAGERESOURCEMANAGER_H
#define PAGERESOURCEMANAGER_H
#include <vector>
#include <string>
#include <memory>
#include "PageResource.h"


namespace Scheduler { namespace HTTP { namespace Pages {

    class PageResourceManager : public PageResource {
        public:
            PageResourceManager(std::string Path = "");
            ~PageResourceManager();
            std::shared_ptr<PageResource> RegisterPage(PageResource* Page);
            bool UnRegister(std::shared_ptr<PageResource> Page);
            Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
            void SetNotFound(PageResource* NotFound);
        private:
            std::vector<std::shared_ptr<PageResource>> m_Pages;
            PageResource* m_NotFoundResource;
    };


} } }

#endif // PAGERESOURCEMANAGER_H
