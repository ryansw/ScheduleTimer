#include "SocketListener.h"

using namespace Scheduler::HTTP::WS;

SocketListener::SocketListener() {
}

SocketListener::~SocketListener() {
}

void SocketListener::WSRecvd(std::string Message, Socket* Socket) {
}

void SocketListener::WSClose(Socket* Socket) {
}

void SocketListener::WSOpen(Socket* Socket) {
}
