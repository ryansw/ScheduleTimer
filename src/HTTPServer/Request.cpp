#include "Request.h"
#include <vector>

using namespace Scheduler::HTTP;

Request::~Request() {
}


Request::Request(http_message* message) {
    Method = MGtoSTD(message->method);
    Uri = MGtoSTD(message->uri);
    Protocol = MGtoSTD(message->proto);
    Query = MGtoSTD(message->query_string);
    Body = MGtoSTD(message->body);
    UriRemain = Uri;
    for(unsigned i = 0; message->header_names[i].len > 0; i++)
        Headers[MGtoSTD(message->header_names[i])] = MGtoSTD(message->header_values[i]);
    Host = Headers["Host"];
    std::string CookieString = Headers["Cookie"];
    while(!CookieString.empty()) {
        unsigned CookieLoc = CookieString.find(';');
        if(CookieLoc > CookieString.size()) CookieLoc = CookieString.size();
        std::string CookieCrumb = CookieString.substr(0, CookieLoc);
        while(CookieLoc < CookieString.size() && CookieString[++CookieLoc] == ' ');
        if(CookieLoc < CookieString.size())
            CookieString = CookieString.substr(CookieLoc, CookieString.size() - CookieLoc);
        else
            CookieString="";
        unsigned c = CookieCrumb.find('=');
        Cookies[CookieCrumb.substr(0, c)]=CookieCrumb.substr(c+1,CookieCrumb.size() - c - 1);
    }
    std::string QueryString = Query;
    while(!QueryString.empty()) {
        unsigned QueryLoc = QueryString.find('&');
        if(QueryLoc > QueryString.size()) QueryLoc = QueryString.size();
        std::string QueryCrumb = QueryString.substr(0, QueryLoc);
        if(QueryLoc < QueryString.size())
            QueryString = QueryString.substr(QueryLoc + 1, QueryString.size() - QueryLoc - 1);
        else
            QueryString="";
        unsigned c = QueryCrumb.find('=');
        Querys[MGUnescape(QueryCrumb.substr(0, c))]=MGUnescape(QueryCrumb.substr(c+1,QueryCrumb.size() - c - 1));
    }
}

std::string Request::MGtoSTD(mg_str in) {
    std::string out (in.p, in.len);
    return out;
}

std::string Request::PrintData() {
    std::string out;
    out = "Protocol: " + Protocol + "\nURI: " + Uri + "\nMethod: " + Method +
            "\nQuery: " + Query + "\nStart Body\n" + Body + "\nEnd Body\nStart Cookies:\n";
    for(auto C : Cookies) out += C.first + ":" + C.second + "\n";
    out += "End Cookies\nStart Querys:\n";
    for(auto Q : Querys) out += Q.first + ":" + Q.second + "\n";
    out += "End Querys\nStart Headers:\n";
    for(auto H : Headers) out += H.first + ":" + H.second + "\n";
    out+="End Headers";
    return out;
}

std::string Request::MGUnescape(std::string in) {
    char* Output = new char[in.size()+1]();
    int ret = mg_url_decode(in.c_str(), in.size(), Output, in.size()+1, 0);
    std::string out = std::string(Output);
    delete Output;
    return out;
}
