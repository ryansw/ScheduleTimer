#include "PageResourceManager.h"
#include "../StaticPages/NoticePage.h"

using namespace Scheduler::HTTP::Pages;

PageResourceManager::PageResourceManager(std::string Path)
    : PageResource(Path, true) {
        m_NotFoundResource = new Scheduler::NoticePage("", "Not Found", "The page you requested cannot be located on the server", 404);
}

PageResourceManager::~PageResourceManager() {
    delete m_NotFoundResource;
}
std::shared_ptr<PageResource> PageResourceManager::RegisterPage(PageResource* Page) {
    std::shared_ptr<PageResource> ret(Page);
    m_Pages.push_back(ret);
    return ret;
}

bool PageResourceManager::UnRegister(std::shared_ptr<PageResource> Page) {
    for(std::vector<std::shared_ptr<PageResource>>::iterator it = m_Pages.begin(); it != m_Pages.end(); ++it) {
        if((*it) == Page) {
            m_Pages.erase(it);
            return true;
        }
    }
    return false;
}

void PageResourceManager::SetNotFound(PageResource* NotFound) {
    delete m_NotFoundResource;
    m_NotFoundResource = NotFound;
}

Scheduler::HTTP::Response PageResourceManager::GetData(Scheduler::HTTP::Request& Req) {
    unsigned i = m_Path.length();
    if(Req.UriRemain.length() > i) i++;
    Req.UriRemain = Req.UriRemain.substr(i, Req.UriRemain.length() - i);
    for(std::vector<std::shared_ptr<PageResource>>::iterator it = m_Pages.begin(); it != m_Pages.end(); ++it) {
        if((*it)->CheckPath(Req.UriRemain))
            return (*it)->GetData(Req);
    }
    return m_NotFoundResource->GetData(Req);
}
