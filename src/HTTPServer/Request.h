#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include "../mongoose.h"
#include <string>
#include <map>

namespace Scheduler {

    namespace HTTP {

        class Request {
            public:
                Request(http_message * message);
                ~Request();
                std::string Method, Uri, Protocol, Query, Body, Host, UriRemain;
                std::map<std::string, std::string> Headers;
                std::map<std::string, std::string> Cookies;
                std::map<std::string, std::string> Querys;
                std::string PrintData();
            private:
                static std::string MGtoSTD(mg_str in);
                static std::string MGUnescape(std::string in);

        };

    }

}

#endif // HTTPREQUEST_H
