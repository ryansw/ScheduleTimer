#include <algorithm>
#include "Socket.h"
#include "SocketListener.h"

using namespace Scheduler::HTTP::WS;

Socket::Socket(mg_connection* Connection, std::string Uri)
    : m_Uri(Uri), m_Conn(Connection), m_Listeners() {
    Connection->user_data = this;
}

Socket::~Socket() {
}

void Socket::AddListener(SocketListener* Listener) {
    m_Listeners.push_back(Listener);
}

void Socket::RemoveListener(SocketListener* Listener) {
    auto Iter = std::find(m_Listeners.begin(), m_Listeners.end(), Listener);
    if(Iter != m_Listeners.end())
        m_Listeners.erase(Iter);
}

void Socket::SendMessage(std::string Message) {
    mg_send_websocket_frame(m_Conn, WEBSOCKET_OP_TEXT, Message.c_str(), Message.length());
}

void Socket::Close() {
    m_Conn->flags |= MG_F_SEND_AND_CLOSE;
}

void Socket::DoDispatchMessage(std::string Message) {
    for(auto L : m_Listeners) L->WSRecvd(Message, this);
}

void Socket::DoDispatchClose() {
    for(auto L : m_Listeners) L->WSClose(this);
}

void Socket::DoDispatchOpen() {
    for(auto L : m_Listeners) L->WSOpen(this);
}
