#ifndef SOCKETLISTENER_H
#define SOCKETLISTENER_H

#include <string>
#include "Socket.h"

namespace Scheduler { namespace HTTP { namespace WS {

    class SocketListener {
        public:
            SocketListener();
            ~SocketListener();
            virtual void WSRecvd(std::string Message, Socket* Socket);
            virtual void WSClose(Socket* socket);
            virtual void WSOpen(Socket* socket);
    };

} } }

#endif // SOCKETLISTENER_H
