#ifndef SOCKET_H
#define SOCKET_H

#include <string>
#include <vector>
#include "../mongoose.h"

namespace Scheduler { namespace HTTP { namespace WS {
    
    class SocketListener;   //Forward Declare
    
    class Socket {
        public:
            Socket(mg_connection* Connection, std::string Uri);
            ~Socket();
            std::string m_Uri;
            void AddListener(SocketListener* Listener);
            void RemoveListener(SocketListener* Listener);
            void SendMessage(std::string Message);
            void Close();
            void DoDispatchMessage(std::string Message);
            void DoDispatchClose();
            void DoDispatchOpen();
        protected:
            mg_connection* m_Conn;
            std::vector<SocketListener*> m_Listeners;
    };

} } }

#endif // SOCKET_H
