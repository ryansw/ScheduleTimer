#include <algorithm>
#include "Server.h"
#include "../StaticPages/BasicFunctionPageResource.h"
#include "Request.h"

using namespace Scheduler::HTTP;

Server::Server(int Port) {
    mg_mgr_init(&Manager, NULL);
    Manager.user_data=this;
    ServerConn = mg_bind(&Manager, std::to_string(Port).c_str(), ev_handler);
    mg_set_protocol_http_websocket(ServerConn);
}

Server::~Server() {
    mg_mgr_free(&Manager);
}

void Server::Poll(int time) {
    mg_mgr_poll(&Manager, time);
}

void Server::PollUntil(std::string path, int time) {
    bool Quit = false;
    auto Hand = RegisterPage(new BasicFunctionPageResource([&Quit]() {
        Quit = true;
    }, path));
    while(!Quit) Poll(time);
    UnRegister(Hand);
}

void Server::ev_handler(mg_connection* nc, int ev, void* ev_data) {
    Server* Parent = static_cast<Server*>(nc->mgr->user_data);
    switch(ev) {
        case MG_EV_HTTP_REQUEST: {
            Request hr (static_cast<struct http_message*>(ev_data));
            Parent->GetData(hr).SendTo(nc);
            nc->flags |= MG_F_SEND_AND_CLOSE;
            break;
        }
        case MG_EV_WEBSOCKET_FRAME: {
            websocket_message* m = static_cast<websocket_message*>(ev_data);
            std::string Message ((char*)(m->data), m->size);
            Parent->m_Sockets[nc]->DoDispatchMessage(Message);
            break;
        }
        case MG_EV_WEBSOCKET_HANDSHAKE_REQUEST: {
            Parent->m_Sockets[nc] = new WS::Socket(nc, Request(static_cast<struct http_message*>(ev_data)).Uri);
            break;
        }
        case MG_EV_WEBSOCKET_HANDSHAKE_DONE: {
            for(auto L : Parent->m_Listeners) L->WSOpen(Parent->m_Sockets[nc]);
            Parent->m_Sockets[nc]->DoDispatchOpen();
            break;
        }
        case MG_EV_CLOSE: {
            if(nc->flags & MG_F_IS_WEBSOCKET) {
                Parent->m_Sockets[nc]->DoDispatchClose();
                for(auto L : Parent->m_Listeners) L->WSClose(Parent->m_Sockets[nc]);
                delete Parent->m_Sockets[nc];
                Parent->m_Sockets.erase(nc);
            }
            break;
        }
        default:
            break;
    }
}

void Server::RegisterSocketListener(WS::SocketListener* Listener) {
    m_Listeners.push_back(Listener);
}

void Server::UnregisterSocketListener(WS::SocketListener* Listener) {
    auto Iter = std::find(m_Listeners.begin(), m_Listeners.end(), Listener);
    if(Iter != m_Listeners.end())
        m_Listeners.erase(Iter);
}
