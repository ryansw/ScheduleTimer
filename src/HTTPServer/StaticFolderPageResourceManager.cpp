#include "StaticFolderPageResourceManager.h"
#include "../StaticPages/NoticePage.h"

#include <fstream>

Scheduler::HTTP::Pages::StaticFolderPageResourceManager::StaticFolderPageResourceManager(std::string Path, std::string Folder)
    : Scheduler::HTTP::Pages::PageResource(Path, true), m_Folder(Folder) {
    m_Mimes["html"] = "text/html";
    m_Mimes["js"] = "text/javascript";
    m_Mimes["css"] = "text/css";
    m_Mimes["wav"] = "audio/wav";
    m_Mimes["svg"] = "image/svg+xml";
    m_NotFound = new Scheduler::NoticePage("", "Not Found", "The page could not be located at the location requested.", 404);
    m_ReadError = new Scheduler::NoticePage("", "System Error", "The page could not be read.", 500);
}

Scheduler::HTTP::Pages::StaticFolderPageResourceManager::~StaticFolderPageResourceManager() {
    delete m_NotFound;
    delete m_ReadError;
}

Scheduler::HTTP::Response Scheduler::HTTP::Pages::StaticFolderPageResourceManager::GetData(Scheduler::HTTP::Request& Req) {
    if(Req.Uri.find("..") <= Req.Uri.length()) return m_ReadError->GetData(Req);
    unsigned i = m_Path.length();
    if(Req.UriRemain.length() > i) i++;
    Req.UriRemain = Req.UriRemain.substr(i, Req.UriRemain.length() - i);
    std::ifstream infile (m_Folder + "/" + Req.UriRemain, std::ios::in | std::ios::binary);
    if(!infile.good()) {
        return m_NotFound->GetData(Req);
    }
    std::string ext = Req.UriRemain.substr(Req.UriRemain.find_last_of(".")+1);
    std::string Content;
    try {
        while(infile.good())
            Content += infile.get();
        Content.pop_back();
    } catch (...) {
        return m_ReadError->GetData(Req);
    }
    infile.close();
    ext = m_Mimes[ext];
    if(ext.length() == 0) ext = "text/plain";
    return Scheduler::HTTP::Response(Content, 200, ext);
}
