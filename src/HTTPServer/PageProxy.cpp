#include "PageProxy.h"

Scheduler::HTTP::Pages::PageProxy::PageProxy(Scheduler::HTTP::Pages::PageResource* receiver, std::string Path, bool CatchChild) 
: Scheduler::HTTP::Pages::PageResource(Path, CatchChild), m_Receiver(receiver) {
}

Scheduler::HTTP::Pages::PageProxy::~PageProxy() {
}

Scheduler::HTTP::Response Scheduler::HTTP::Pages::PageProxy::GetData(Scheduler::HTTP::Request& Req) {
    Req.UriRemain = m_Receiver->m_Path + Req.UriRemain;
    return m_Receiver->GetData(Req);
}
