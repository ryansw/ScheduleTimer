#ifndef PAGEMODIFIER_H
#define PAGEMODIFIER_H
#include <string>
#include "Request.h"
#include "Response.h"
#include "PageResource.h"

namespace Scheduler { namespace HTTP { namespace Pages {

    class PageModifier : public PageResource {
        public:
            PageModifier(PageResource* ChildResource);
            virtual ~PageModifier();
            Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
            virtual Scheduler::HTTP::Response ModifyData(Scheduler::HTTP::Request& Req, Scheduler::HTTP::Response Resp) =0;
        protected:
            PageResource* m_Resource;
    };

} } }

#endif // PAGEMODIFIER_H
