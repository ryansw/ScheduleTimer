#include "PageModifier.h"

Scheduler::HTTP::Pages::PageModifier::PageModifier(PageResource* ChildResource) 
: Scheduler::HTTP::Pages::PageResource(ChildResource->m_Path, ChildResource->m_CatchChild), m_Resource(ChildResource) {

}

Scheduler::HTTP::Pages::PageModifier::~PageModifier() {
    delete m_Resource;
}

Scheduler::HTTP::Response Scheduler::HTTP::Pages::PageModifier::GetData(Scheduler::HTTP::Request& Req) {
    return ModifyData(Req, m_Resource->GetData(Req));
}
