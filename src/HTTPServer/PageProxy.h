#ifndef PAGEPROXY_H
#define PAGEPROXY_H

#include "PageResource.h" // Base class: Scheduler::HTTP::Pages::PageResource

namespace Scheduler { namespace HTTP { namespace Pages {

    class PageProxy : public Scheduler::HTTP::Pages::PageResource {
        public:
            PageProxy(Scheduler::HTTP::Pages::PageResource* receiver, std::string Path, bool CatchChild = true);
            Scheduler::HTTP::Pages::PageResource* m_Receiver;
            ~PageProxy();

        public:
            virtual Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
    };

} } }

#endif // PAGEPROXY_H
