#include "HearderPageModifier.h"

Scheduler::HearderPageModifier::HearderPageModifier(Scheduler::HTTP::Pages::PageResource* res) 
    : Scheduler::HTTP::Pages::PageModifier(res) {
}

Scheduler::HearderPageModifier::~HearderPageModifier() {
}

Scheduler::HTTP::Response Scheduler::HearderPageModifier::ModifyData(Scheduler::HTTP::Request& Req, Scheduler::HTTP::Response Resp) {
    std::string Body = Resp.GetBody();
    for(auto pair : Req.Headers) {
        std::string key = "$(" + pair.first + ")";
        while(Body.find(key) != std::string::npos)
            Body = Body.replace(Body.find(key), key.length(), pair.second);
    }
    Resp.SetBody(Body);
    return Resp;
}
