#ifndef SYSTEMADAPTER_H
#define SYSTEMADAPTER_H

#include <string>
#include "HTTPServer/PageResource.h"
#include "HTTPServer/PageProxy.h"
#include "HTTPServer/PageResourceManager.h"
#include "ScheduleManagement/System.h"
#include "SystemClient.h"
#include <vector>

namespace Scheduler {

    class SystemAdapter : public HTTP::Pages::PageResourceManager {
        public:
            SystemAdapter(System* Sys);
            ~SystemAdapter();
            std::string GetPath();
            std::string GetData();
            std::string GetCurrentSchedule();
            void MakeClient(Scheduler::HTTP::WS::Socket* sock);
            void RemoveClient(Scheduler::SystemClient* client);
        private:
            HTTP::Pages::PageProxy* m_Landing;
            HTTP::Pages::PageResource * m_ViewNormal, * m_ViewDiag, * m_ViewLegacy;
            System* m_System;
            std::vector<Scheduler::SystemClient*> m_Clients;
    };

}

#endif // SYSTEMADAPTER_H
