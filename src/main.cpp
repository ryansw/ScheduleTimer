#include "Program.h"
#include <iostream>


#include <odb/database.hxx>
#include <odb/sqlite/database.hxx>
#include <odb/schema-catalog.hxx>
#include "ScheduleManagement/System-odb.hxx"
#include "ScheduleManagement/Schedule-odb.hxx"
#include "ScheduleManagement/Event-odb.hxx"

#include "ScheduleManagement/SchedUtil.h"

using namespace Scheduler;

Scheduler::System* GenerateTest(int Gap) {
    Scheduler::System* sys = new Scheduler::System("Gap Test " + std::to_string(Gap) + " Minutes", "Test" + std::to_string(Gap));
    int stop = 24 * 60 * 60;
    Scheduler::Schedule* sched = new Scheduler::Schedule("Gap Test", std::to_string(Gap));
    for(int i = 0; i * Gap * 60 < stop; i++) {
        sched->AddEvent(new Scheduler::Event("Event " + std::to_string(i), i * Gap * 60, "Event " + std::to_string(i)));
    }
    sys->AddSchedule(sched);
    return sys;
}

int main() {
    std::auto_ptr<odb::database> db ( new odb::sqlite::database("test.db", SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE));
    if(false) {
        odb::transaction t (db->begin ());
        odb::schema_catalog::create_schema(*db);
        t.commit();
    }
//    { // Test generation
//        odb::transaction t (db->begin());
//        std::vector<Scheduler::System*> sysToAdd;
//        sysToAdd.push_back(GenerateTest(1));
//        sysToAdd.push_back(GenerateTest(5));
//        sysToAdd.push_back(GenerateTest(10));
//        for(Scheduler::System* sys : sysToAdd) {
//            for (auto sched : sys->OrderedSchedules) {
//                for (auto even : sched->Events) {
//                    db->persist(even.get());
//                }
//                db->persist(sched.get());
//            }
//            db->persist(sys);
//        }
//        t.commit();
//    }
//    return 1;

//    { //Modification Test
//        odb::transaction t (db->begin());
//        odb::result<Scheduler::System> r (db->query<Scheduler::System>(
//            odb::query<Scheduler::System>::Path == "svhs"
//        ));
//        for(auto i (r.begin()); i != r.end(); i++) {
//            Scheduler::System* sys = i.load ();
//            std::cout << sys->Name << std::endl;
//            Scheduler::Schedule* AssemblyDay = new Scheduler::Schedule("Assembly Day", "Homeroom Assembly", "Assembly Day");
//            AssemblyDay->AddEvent(new Scheduler::Event("First Period Starts",26400,"First Period","First Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("First Period Ends",29400,"First Period over","First Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Second Period Starts",29700,"Second Period","Second Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Second Period Ends",32700,"Second Period over","Second Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Third Period Starts",33000,"Third Period","Third Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Third Period Ends",36000,"Third Period over","Third Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Fourth Period Starts",36300,"Fourth Period","Fourth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Fourth Period Ends",39300,"Fourth Period over","Fourth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Fifth Period Starts",39600,"Fifth Period","Fifth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Fifth Period Ends",42600,"Fifth Period over","Fifth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Lunch Ends",44400,"Lunch over","Lunch"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Sixth Period Starts",44700,"Sixth Period","Sixth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Sixth Period Ends",47700,"Sixth Period over","Sixth Period"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Homeroom Assembly Starts",48000,"Homeroom Assembly","Homeroom Assembly"));
//            AssemblyDay->AddEvent(new Scheduler::Event("Homeroom Assembly Ends",51900,"Homeroom Assembly over","Homeroom Assembly"));
//            AssemblyDay->DisplayOnDate(Scheduler::TimeUtil::getDate(-7));
//            AssemblyDay->DisplayReason = Scheduler::Schedule::DisplayClassifier::Dates;
//            for(auto e : AssemblyDay->Events) {
//                db->persist(e.get());
//            }
//            db->persist(AssemblyDay);
//            sys->AddSchedule(AssemblyDay);
//            db->update(sys);
//        }
//        t.commit();
//    }
//    return 1;

    Program p(8111);
    {
        odb::transaction t (db->begin());
        odb::result<Scheduler::System> r (db->query<Scheduler::System>());
        if(r.empty()) std::cout << "No objects!!" << std::endl;
        else {
            for(auto i (r.begin()); i != r.end(); i++) {
                Scheduler::System* sys = i.load ();
                p.RegisterSystem(sys);
                std::cout << SystemAdapter(sys).GetData() << std::endl;
            }
        }
        t.commit();
    }
    p.MakeDefaultPath("Test5");
    p.Run();
}
