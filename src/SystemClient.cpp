#include "SystemClient.h"
#include "SystemAdapter.h"

#include <iostream>


Scheduler::SystemClient::SystemClient(Scheduler::HTTP::WS::Socket* Sock, Scheduler::SystemAdapter* Adapter)
    :m_Sock(Sock), m_Adapter(Adapter) {
        m_Sock->AddListener(this);
        m_Sock->SendMessage("Hello!");
        std::cout << "Created!" << std::endl;
}

Scheduler::SystemClient::~SystemClient() {
    m_Adapter->RemoveClient(this);
        std::cout << "Destroyed!" << std::endl;
}

void Scheduler::SystemClient::WSRecvd(std::string Message, Scheduler::HTTP::WS::Socket* Socket) {
    m_Sock->SendMessage(Message);
}

void Scheduler::SystemClient::WSClose(Scheduler::HTTP::WS::Socket* Socket) {
    delete this;
}
