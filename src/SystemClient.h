#ifndef SYSTEMCLIENT_H
#define SYSTEMCLIENT_H

#include "HTTPServer/Socket.h" 
#include "HTTPServer/SocketListener.h" // Base class: Scheduler::HTTP::WS::SocketListener

namespace Scheduler {
    
    class SystemAdapter;    //Forward declaration
    
    class SystemClient : public Scheduler::HTTP::WS::SocketListener {
        public:
            SystemClient(Scheduler::HTTP::WS::Socket* Sock, Scheduler::SystemAdapter* Adapter);
            ~SystemClient();
        private:
            Scheduler::HTTP::WS::Socket* m_Sock;
            Scheduler::SystemAdapter* m_Adapter;
            void WSRecvd(std::string Message, Scheduler::HTTP::WS::Socket* Socket);
            void WSClose(Scheduler::HTTP::WS::Socket* Socket);

    };

}

#endif // SYSTEMCLIENT_H
