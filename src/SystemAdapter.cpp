#include "SystemAdapter.h"
#include "json.hpp"
#include "StaticPages/ReturnFunctionPageResource.h"
#include "StaticPages/FilePageResource.h"
#include "VariableReplacementModifier.h"
#include "HearderPageModifier.h"
#include "ScheduleManagement/SchedUtil.h"
#include <algorithm>

nlohmann::basic_json<> GetScheduleData(Scheduler::Schedule * sched, bool DWDInfo = false);

std::string GetLegacyData(Scheduler::System * sys);

Scheduler::SystemAdapter::SystemAdapter(System* Sys)
    : Scheduler::HTTP::Pages::PageResourceManager(Sys->Path), m_System(Sys) {
    RegisterPage(new ReturnFunctionPageResource(std::bind(&Scheduler::SystemAdapter::GetData, this), "data.json", 200, "application/json"));
    RegisterPage(new ReturnFunctionPageResource(std::bind(&GetLegacyData, this->m_System), "legacy.json", 200, "application/json"));
    RegisterPage( new HearderPageModifier( 
            new BasicPageResource(
                "var Name=\"" + m_System->Name +
                "\",Description=\"" + m_System->Description +
                "\",Path=\"" + m_System->Path +
                "\",TimeZone=" + std::to_string(m_System->TimeZoneOffset) +
                ",Host=\"$(Host)\";",
            "vars.js", 200, "text/javascript")));
    std::map<std::string, std::string> variables;
    variables["NAME"] = m_System->Name;
    variables["DESCRIPTION"] = m_System->Description;
    variables["PATH"] = m_System->Path;
    variables["TIMEZONE"] = std::to_string(m_System->TimeZoneOffset);
    m_ViewNormal =  new HearderPageModifier(
                new VariableReplacementModifier(
                new FilePageResource("viewer", "html/viewer.html", 200, false), variables));
    m_ViewDiag = new HearderPageModifier(
                new VariableReplacementModifier(
                new FilePageResource("diagnostics", "html/diagnostics.html", 200, false), variables));
    m_ViewLegacy = new HearderPageModifier(
                new VariableReplacementModifier(
                new FilePageResource("legacy", "html/legacy.html", 200, false), variables));
    m_Landing = new Scheduler::HTTP::Pages::PageProxy(m_ViewNormal, "");
    RegisterPage(m_ViewNormal);
    RegisterPage(m_ViewDiag);
    RegisterPage(m_ViewLegacy);
    RegisterPage(m_Landing);
}

std::string Scheduler::SystemAdapter::GetPath() {
    return m_System->Path;
}

std::string Scheduler::SystemAdapter::GetData() {
    nlohmann::basic_json<> Data;
    Data["Name"]=m_System->Name;
    Data["Path"]=m_System->Path;
    Data["Description"]=m_System->Description;
    Data["Timezone"]=m_System->TimeZoneOffset;
    nlohmann::basic_json<> Schedules = nlohmann::basic_json<>::array();
    for(auto s : m_System->OrderedSchedules) {
        Schedules.push_back(GetScheduleData(s.get(), true));
    }
    Data["Schedules"] = Schedules;
    return Data.dump();
}

std::string Scheduler::SystemAdapter::GetCurrentSchedule() {
    return GetScheduleData(m_System->GetCurrentSchedule()).dump();
}

nlohmann::basic_json<> GetScheduleData(Scheduler::Schedule * sched, bool DWDInfo) {
    nlohmann::basic_json<> Events = nlohmann::basic_json<>::array();
    for(auto e: sched->Events) {
        nlohmann::basic_json<> event;
        event["Name"] = e->Name;
        event["Description"] = e->Description;
        event["Message"] = e->Message;
        event["Time"] = e->Time;
        Events.push_back(event);
    }
    nlohmann::basic_json<> Data;
    Data["Name"] = sched->Name;
    Data["DisplayName"] = sched->DisplayName;
    Data["Description"] = sched->Description;
    if(DWDInfo) {
        Data["WDays"] = sched->WeekDaysToDisplay;
        nlohmann::basic_json<> Dates = nlohmann::basic_json<>::array();
        for(auto d : sched->DatesToDisplay)
            Dates.push_back(d);
        Data["Dates"] = Dates;
        Data["Reason"] = sched->DisplayReason;
    }
    Data["Events"] = Events;
    return Data;
}

void Scheduler::SystemAdapter::MakeClient(Scheduler::HTTP::WS::Socket* sock) {
    m_Clients.push_back(new Scheduler::SystemClient(sock, this));
}

void Scheduler::SystemAdapter::RemoveClient(Scheduler::SystemClient* client) {
    m_Clients.erase(std::find(m_Clients.begin(), m_Clients.end(), client));
}

Scheduler::SystemAdapter::~SystemAdapter() {
}

std::string GetLegacyData(Scheduler::System * sys) {
    nlohmann::basic_json<> Schedules = nlohmann::basic_json<>::array();
    for (auto schedIter = sys->OrderedSchedules.rbegin(); schedIter != sys->OrderedSchedules.rend(); schedIter++) {
        auto sched = *schedIter;
        nlohmann::basic_json<> ScheduleJSON = nlohmann::basic_json<>::object();
        ScheduleJSON["title"] = sched->DisplayName;
        nlohmann::basic_json<> Days = nlohmann::basic_json<>::array();
        for(int i = 0; i < 7; i++) {
            if(sched->WeekDaysToDisplay & 1<<i) Days.push_back(i);
        }
        ScheduleJSON["day"] = Days;
        nlohmann::basic_json<> Dates = nlohmann::basic_json<>::array();
        for(unsigned int Date : sched->DatesToDisplay) {
            Dates.push_back(Scheduler::TimeUtil::hrDate(Date, false));
        }
        ScheduleJSON["date"] = Dates;
        nlohmann::basic_json<> Alarms = nlohmann::basic_json<>::array();
        for(auto even : sched->Events) {
            nlohmann::basic_json<> EventJson = nlohmann::basic_json<>::object();
            EventJson["message"] = even->Name;
            EventJson["notice"] = even->Message;
            int T = even->Time;
            int s = T % 60; T = (T-s) / 60;
            int m = T % 60; T = (T-m) / 60;
            int h = T % 24;
            std::string H = (h < 10 ? "0" : "") + std::to_string(h);
            std::string M = (m < 10 ? "0" : "") + std::to_string(m);
            std::string S = (s < 10 ? "0" : "") + std::to_string(s);
            EventJson["time"] = nlohmann::basic_json<>::array({ H, M, S});
            Alarms.push_back(EventJson);
        }
        ScheduleJSON["alarms"] = Alarms;
        Schedules.push_back(ScheduleJSON);
    }
    return Schedules.dump();
}