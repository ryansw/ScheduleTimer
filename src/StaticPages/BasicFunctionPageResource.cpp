#include "BasicFunctionPageResource.h"

Scheduler::BasicFunctionPageResource::BasicFunctionPageResource(std::function<void()> callback, std::string path, std::string content, bool CatchChild)
    :Scheduler::BasicPageResource(content, path, 200, "text/plain", CatchChild) {
    m_callback = callback;
}

Scheduler::BasicFunctionPageResource::~BasicFunctionPageResource() {
}

Scheduler::HTTP::Response Scheduler::BasicFunctionPageResource::GetData(Scheduler::HTTP::Request& Req) {
    m_callback();
    Scheduler::HTTP::Response Resp(m_content);
    return Resp;
}

