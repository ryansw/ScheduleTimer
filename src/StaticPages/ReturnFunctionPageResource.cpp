#include "ReturnFunctionPageResource.h"

Scheduler::ReturnFunctionPageResource::ReturnFunctionPageResource(std::function<std::string()> callback, std::string path, int respcode, std::string mime,  bool CatchChild)
    :Scheduler::BasicPageResource("", path, respcode, mime, CatchChild) {
    m_callback = callback;
}

Scheduler::ReturnFunctionPageResource::~ReturnFunctionPageResource() {
}

Scheduler::HTTP::Response Scheduler::ReturnFunctionPageResource::GetData(Scheduler::HTTP::Request& Req) {
    return Scheduler::HTTP::Response(m_callback(), m_ResponseCode, m_mime);
}
