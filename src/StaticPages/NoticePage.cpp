#include "NoticePage.h"

using namespace Scheduler;


NoticePage::NoticePage(std::string Path, std::string Title, std::string Body, int ResponseCode, bool CatchChild)
    : HTTP::Pages::PageResource(Path, CatchChild), m_Title(Title), m_Body(Body), m_ResponseCode(ResponseCode) {
}

NoticePage::~NoticePage() {
}

HTTP::Response NoticePage::GetData(HTTP::Request& Req) {
    std::string Data;
    Data = "<!DOCTYPE html><html><head><title>" + m_Title + "</title></head><body><h1>" + m_Title
        + "</h1>" + m_Body + "<hr /><h6>RyanSW Server Message</h6></body></html>";
    return HTTP::Response(Data, m_ResponseCode);
}
