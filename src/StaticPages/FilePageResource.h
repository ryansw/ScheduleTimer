#ifndef FILEPAGERESOURCE_H
#define FILEPAGERESOURCE_H

#include "../HTTPServer/PageResource.h"

namespace Scheduler {

    class FilePageResource : public HTTP::Pages::PageResource {
        public:
            FilePageResource(std::string Path, std::string File, int ResponseCode = 200, bool CatchChild = true);
            ~FilePageResource();
            virtual HTTP::Response GetData(HTTP::Request& Req);
            std::string m_File;
            int m_ResponseCode;
    };

}


#endif // FILEPAGERESOURCE_H
