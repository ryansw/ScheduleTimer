#ifndef BASICFUNCTIONPAGERESOURCE_H
#define BASICFUNCTIONPAGERESOURCE_H
#include <functional>
#include "BasicPageResource.h"

namespace Scheduler {
    class BasicFunctionPageResource : public Scheduler::BasicPageResource {
        public:
            BasicFunctionPageResource(std::function<void()> callback, std::string path, std::string content = "", bool CatchChild = true);
            ~BasicFunctionPageResource();

        public:
            Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
        private:
            std::function<void()> m_callback;
    };

}

#endif // BASICFUNCTIONPAGERESOURCE_H
