#include "BasicPageResource.h"

Scheduler::BasicPageResource::BasicPageResource(std::string content, std::string path, int respcode, std::string mime,  bool CatchChild)
    : Scheduler::HTTP::Pages::PageResource(path, CatchChild), m_content(content), m_ResponseCode(respcode), m_mime(mime) {

}

Scheduler::BasicPageResource::~BasicPageResource() {
}

Scheduler::HTTP::Response Scheduler::BasicPageResource::GetData(Scheduler::HTTP::Request& Req) {
    return Scheduler::HTTP::Response(m_content, m_ResponseCode, m_mime);
}
