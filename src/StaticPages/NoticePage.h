#ifndef NOTICEPAGE_H
#define NOTICEPAGE_H
#include <string>
#include "../HTTPServer/PageResource.h" 

namespace Scheduler { 

    class NoticePage : public HTTP::Pages::PageResource {
        public:
            NoticePage(std::string Path, std::string Title, std::string Body, int ResponseCode, bool CatchChild = true);
            ~NoticePage();
            virtual HTTP::Response GetData(HTTP::Request& Req);
            std::string m_Title, m_Body;
            int m_ResponseCode;
    };

}

#endif // NOTICEPAGE_H
