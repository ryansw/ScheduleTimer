#ifndef BASICPAGERESOURCE_H
#define BASICPAGERESOURCE_H

#include "../HTTPServer/PageResource.h"

namespace Scheduler {

    class BasicPageResource : public Scheduler::HTTP::Pages::PageResource {
        public:
            BasicPageResource(std::string content, std::string path, int respcode=200, std::string mime="text/html", bool CatchChild = true);
            virtual ~BasicPageResource();

        public:
            virtual Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
        protected:
            std::string m_content;
            int m_ResponseCode;
            std::string m_mime;
    };

}

#endif // BASICPAGERESOURCE_H
