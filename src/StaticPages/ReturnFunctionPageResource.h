#ifndef RETURNFUNCTIONPAGERESOURCE_H
#define RETURNFUNCTIONPAGERESOURCE_H
#include <string>
#include <functional>
#include "BasicPageResource.h" // Base class: Scheduler::BasicPageResource

namespace Scheduler {
    class ReturnFunctionPageResource : public Scheduler::BasicPageResource {
        public:
            ReturnFunctionPageResource(std::function<std::string()> callback, std::string path, int respcode=200, std::string mime="text/html", bool CatchChild = true);
            ~ReturnFunctionPageResource();
            Scheduler::HTTP::Response GetData(Scheduler::HTTP::Request& Req);
        private:
            std::function<std::string()> m_callback;
    };

}

#endif // RETURNFUNCTIONPAGERESOURCE_H
