#include "FilePageResource.h"
#include <fstream>
#include "NoticePage.h"

Scheduler::FilePageResource::FilePageResource(std::string Path, std::string File, int ResponseCode, bool CatchChild)
    : Scheduler::HTTP::Pages::PageResource(Path, CatchChild), m_File(File), m_ResponseCode(ResponseCode) {
}

Scheduler::HTTP::Response Scheduler::FilePageResource::GetData(HTTP::Request& Req) {
    try {
        std::ifstream InputStream(m_File, std::ifstream::in);
        std::string Content;
        while(InputStream.good())
            Content += InputStream.get();
        Content.pop_back();
        InputStream.close();
        return HTTP::Response(Content, m_ResponseCode);
    } catch (...) {
        return Scheduler::NoticePage("", "System Error", "The system could not read the file requested.", 500).GetData(Req);
    }
}

Scheduler::FilePageResource::~FilePageResource() {
}
