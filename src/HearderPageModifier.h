#ifndef HEARDERPAGEMODIFIER_H
#define HEARDERPAGEMODIFIER_H

#include "HTTPServer/PageModifier.h" // Base class: Scheduler::HTTP::Pages::PageModifier

namespace Scheduler {

    class HearderPageModifier : public Scheduler::HTTP::Pages::PageModifier {
        public:
            HearderPageModifier(Scheduler::HTTP::Pages::PageResource* res);
            ~HearderPageModifier();

        public:
            virtual Scheduler::HTTP::Response ModifyData(Scheduler::HTTP::Request& Req, Scheduler::HTTP::Response Resp);
    };

}

#endif // HEARDERPAGEMODIFIER_H
