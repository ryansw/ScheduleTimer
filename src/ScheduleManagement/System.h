#ifndef SYSTEM_H
#define SYSTEM_H
#include <odb/core.hxx>

#include <string>
#include <vector>
#include <memory>
#include "Schedule.h"

#pragma db model version(1, 1)

namespace Scheduler {
    #pragma db object
    class System {
        public:
            std::string Name;
            #pragma db unique
            std::string Path;
            std::string Description;
            int TimeZoneOffset;
            std::vector<std::shared_ptr<Schedule>> OrderedSchedules;
            System(std::string name, std::string path, std::string description = "", int timezoneoffset = 0);
            void AddSchedule(Schedule* ToAdd);
            Schedule* GetCurrentSchedule();
            Event* GetNextEvent();
            ~System();
        private:
            friend class odb::access;
            System();
            #pragma db id auto
            unsigned long id_;
            #pragma db transient
            Schedule None;

    };

}

#endif // SYSTEM_H
