#ifndef EVENT_H
#define EVENT_H
#include <odb/core.hxx>

#include <string>

#pragma db model version(1, 1)

namespace Scheduler {
    #pragma db object
    class Event {
        public:
            Event(std::string name, int time, std::string message = "", std::string description = "");
            std::string Name;
            int Time;
            std::string Message, Description;
            ~Event();
        private:
            friend class odb::access;
            Event();
            #pragma db id auto
            unsigned long id_;
    };

    namespace EventUtil {
        bool CompareTimes(Event a, Event b);
    }

}

#endif // EVENT_H
