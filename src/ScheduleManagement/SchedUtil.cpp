#include "SchedUtil.h"
#include <ctime>
#include <sstream>
#include <memory>

std::unique_ptr<struct tm> GetGMTime(int utcoffset) {
    time_t timeA = time(0) + (utcoffset) * 3600 ;
    struct tm * gmA = new struct tm;
    gmtime_r(&timeA, gmA);
    return std::unique_ptr<struct tm>(gmA);
}

int Scheduler::TimeUtil::getTime(int utcoffset) {
    std::unique_ptr<struct tm> gm = GetGMTime(utcoffset);
    return gm->tm_sec + 60 * (gm->tm_min + 60 * gm->tm_hour);
}

int Scheduler::TimeUtil::getWDay(int utcoffset) {
    std::unique_ptr<struct tm> gm = GetGMTime(utcoffset);
    return gm->tm_wday;
}

int Scheduler::TimeUtil::getDate(int utcoffset) {
    std::unique_ptr<struct tm> gm = GetGMTime(utcoffset);
    return gm->tm_mday + 100 * (gm->tm_mon + 100 * gm->tm_year);
}


std::string Scheduler::TimeUtil::hrTime(int TimeOfDay) {
    std::stringstream build;
    int sec = TimeOfDay % 60, min = (TimeOfDay = (TimeOfDay -sec) / 60) % 60, hrs = (TimeOfDay = (TimeOfDay - min) / 60);

    if(hrs < 10) build << "0";

    build << hrs <<":";

    if(min < 10) build << "0";

    build << min <<":";

    if(sec < 10) build << "0";

    build << sec;
    return build.str();
}

std::string Scheduler::TimeUtil::hrDate(int DateCode, bool Pad) {
    std::stringstream build;
    int day = DateCode % 100, mon = (DateCode = (DateCode - day) / 100) % 100, year = (DateCode = (DateCode - mon) / 100) +1900;
    mon++;
    if(mon < 10 && Pad) build << "0";

    build << mon << "-";

    if(day < 10 && Pad) build << "0";

    build << day << "-" << year;
    return build.str();
}
