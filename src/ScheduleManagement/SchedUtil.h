#ifndef SCHEDUTIL_H
#define SCHEDUTIL_H

#include <string>

namespace Scheduler {

    namespace TimeUtil {
        int getTime(int utcoffset);
        int getWDay(int utcoffset);
        int getDate(int utcoffset);
        std::string hrTime(int TimeOfDay);
        std::string hrDate(int DateCode, bool Pad = true);
    }

}

#endif // SCHEDUTIL_H
