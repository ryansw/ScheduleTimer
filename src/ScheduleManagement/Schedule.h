#ifndef SCHEDULE_H
#define SCHEDULE_H
#include <odb/core.hxx>

#include <string>
#include <vector>
#include <memory>
#include "Event.h"

#pragma db model version(1, 1)

namespace Scheduler {
    #pragma db object
    class Schedule {
        public:
            enum DisplayClassifier {
                Always, WeekDays, Dates
            };
            std::string Name, DisplayName, Description;
            short unsigned int WeekDaysToDisplay;
            std::vector<unsigned int> DatesToDisplay;
            DisplayClassifier DisplayReason;
            std::vector<std::shared_ptr<Event>> Events;
            Schedule(std::string name, std::string displayname, std::string description = "");
            void AddEvent(Event* ToAdd);
            void RemoveEvent(Event* E);
            void DisplayOnWeek(unsigned int WeekDay, bool DoDisplay = true);
            void DisplayOnDate(unsigned int DateToDisplay);
            void ClearDisplayReasons();
            Event* GetNextEvent(int Time);
            ~Schedule();
        private:
            friend class odb::access;
            Schedule();
            #pragma db id auto
            unsigned long id_;
            #pragma db transient
            Event None;
    };
}

#endif // SCHEDULE_H
