#include "System.h"
#include <algorithm>
#include "SchedUtil.h"

Scheduler::System::~System() {
}

Scheduler::System::System(std::string name, std::string path, std::string description, int timezoneoffset)
    : Name(name), Path(path), Description(description), TimeZoneOffset(timezoneoffset), OrderedSchedules(), None("None", "None", "None"){
}

void Scheduler::System::AddSchedule(Schedule* ToAdd) {
    OrderedSchedules.push_back(std::shared_ptr<Scheduler::Schedule>(ToAdd));
}

Scheduler::Schedule* Scheduler::System::GetCurrentSchedule() {
    for(auto Sch : OrderedSchedules) {
        if(Sch->DisplayReason == Scheduler::Schedule::Always ||
                (Sch->DisplayReason == Scheduler::Schedule::WeekDays && (Sch->WeekDaysToDisplay & (1 << Scheduler::TimeUtil::getWDay(TimeZoneOffset)))) ||
                (Sch->DisplayReason == Scheduler::Schedule::Dates && std::find(Sch->DatesToDisplay.begin(), Sch->DatesToDisplay.end(), Scheduler::TimeUtil::getDate(TimeZoneOffset)) != Sch->DatesToDisplay.end()))
            return Sch.get();
    }
    return &None;
}

Scheduler::Event* Scheduler::System::GetNextEvent() {
    return GetCurrentSchedule()->GetNextEvent(Scheduler::TimeUtil::getTime(TimeZoneOffset));
}

Scheduler::System::System() : None("None", "None", "None") {}
