#include "Schedule.h"

Scheduler::Schedule::~Schedule() {
}

Scheduler::Schedule::Schedule(std::string name, std::string displayname, std::string description)
    :Name(name), DisplayName(displayname), Description(description), WeekDaysToDisplay(0), DatesToDisplay(), DisplayReason(Schedule::Always), None("None", 0, "None", "None") {
}

void Scheduler::Schedule::AddEvent(Scheduler::Event* ToAdd) {
    Events.push_back(std::shared_ptr<Scheduler::Event>(ToAdd));
}

void Scheduler::Schedule::RemoveEvent(Event* E) {
    for(auto i = Events.begin(); i != Events.end(); i++) {
        if(i->get() == E) {
            Events.erase(i);
            return;
        }
    }
}

void Scheduler::Schedule::DisplayOnWeek(unsigned int WeekDay, bool DoDisplay) {
    if(DoDisplay)
        WeekDaysToDisplay |= (1 << WeekDay);
    else
        WeekDaysToDisplay &= ~(1<<WeekDay);
}

void Scheduler::Schedule::DisplayOnDate(unsigned int DateToDisplay) {
    DatesToDisplay.push_back(DateToDisplay);
}

void Scheduler::Schedule::ClearDisplayReasons() {
    DatesToDisplay.clear();
    WeekDaysToDisplay = 0;
}


Scheduler::Event* Scheduler::Schedule::GetNextEvent(int Time) {

    for(auto eve : Events) {
        if(eve->Time > Time)
            return eve.get();
    }

    return &None;
}

Scheduler::Schedule::Schedule() : None("None", 0, "None", "None") {}
