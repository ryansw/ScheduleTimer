#include "Program.h"
#include "HTTPServer/StaticFolderPageResourceManager.h"

Scheduler::Program::Program(int Port)
    : m_Server(Port) {
        m_Server.RegisterPage(new Scheduler::HTTP::Pages::StaticFolderPageResourceManager("static", "static"));
        m_Server.RegisterSocketListener(this);
        m_Landing = NULL;
}

void Scheduler::Program::RegisterSystem(System* sys) {
    SystemAdapter* Adapt = new SystemAdapter(sys);
    m_Adapters[Adapt->GetPath()] = Adapt;
    m_Server.RegisterPage(Adapt);
}

void Scheduler::Program::Run() {
    m_Server.PollUntil("kill");
}

void Scheduler::Program::WSOpen(Scheduler::HTTP::WS::Socket* socket) {
    if(socket->m_Uri.length() <= 4) {
        socket->Close();
        return;
    }
    socket->m_Uri = socket->m_Uri.substr(4, socket->m_Uri.length() - 4);
    try {
        m_Adapters.at(socket->m_Uri)->MakeClient(socket);
    } catch (std::out_of_range& e) {
        socket->Close();
        return;
    }
}

Scheduler::Program::~Program() {
    delete m_Landing;
}

void Scheduler::Program::MakeDefaultPath(std::string Path) {
    Scheduler::HTTP::Pages::PageResource* NewLoc = NULL;
    try {
        NewLoc = m_Adapters.at(Path);
    } catch (std::out_of_range& e) { return; }
    if(NewLoc != NULL) {
        if(m_Landing == NULL){
            m_Landing = new Scheduler::HTTP::Pages::PageProxy(NewLoc, "");
            m_Server.RegisterPage(m_Landing);
        } else {
            m_Landing->m_Receiver = NewLoc;
        }
    }
}
